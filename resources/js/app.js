import 'es6-promise/auto'
import axios from 'axios'
import './bootstrap'
import Vue from 'vue'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import App from './components/App'
import auth from './auth'
import router from './router'
import http from   '@websanova/vue-auth/drivers/http/vue-resource.1.x.js'

// Set Vue globally
window.Vue = Vue
// Set Vue router
Vue.router = router
Vue.use(VueRouter)
// Set Vue authentication
Vue.use(VueAxios, axios)
Vue.router = router

// Vue.http = http
// Vue.http.options.root = URL
axios.defaults.baseURL = `http://127.0.0.1:8000/api`
Vue.use(VueAuth, auth)
// const URL = window.location.protocol + '//' + window.location.hostname + '/'


Vue.component('App', App)
const app = new Vue({
    el: '#app',
    router
});
